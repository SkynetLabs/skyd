package main

import (
	"math/big"
	"testing"

	"github.com/andreyvit/diff"
	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/types"
)

// TestParseLSArgs probes the parseLSArgs function
func TestParseLSArgs(t *testing.T) {
	t.Parallel()

	// Define Tests
	var tests = []struct {
		args []string
		sp   skymodules.SiaPath
		err  error
	}{
		// Valid Cases
		{nil, skymodules.RootSiaPath(), nil},
		{[]string{}, skymodules.RootSiaPath(), nil},
		{[]string{""}, skymodules.RootSiaPath(), nil},
		{[]string{"."}, skymodules.RootSiaPath(), nil},
		{[]string{"/"}, skymodules.RootSiaPath(), nil},
		{[]string{"path"}, skymodules.SiaPath{Path: "path"}, nil},

		// Invalid Cases
		{[]string{"path", "extra"}, skymodules.SiaPath{}, errIncorrectNumArgs},
		{[]string{"path", "extra", "extra"}, skymodules.SiaPath{}, errIncorrectNumArgs},
		{[]string{"...//////badd....////path"}, skymodules.SiaPath{}, skymodules.ErrInvalidSiaPath},
	}
	// Execute Tests
	for _, test := range tests {
		sp, err := parseLSArgs(test.args)
		if !sp.Equals(test.sp) {
			t.Log("Expected:", test.sp)
			t.Log("Actual:", sp)
			t.Error("unexpected siapath")
		}
		if !errors.Contains(err, test.err) && err != test.err {
			t.Log("Expected:", test.err)
			t.Log("Actual:", err)
			t.Error("unexpected error")
		}
	}
}

// TestCurrentPeriodSpending is a small unit test that verifies the output of
// the currentperiodspending helper
func TestCurrentPeriodSpending(t *testing.T) {
	t.Parallel()

	fm := skymodules.FinancialMetrics{
		ContractorSpending: skymodules.ContractorSpending{
			DownloadSpending: types.SiacoinPrecision.Mul64(1),
			Fees: skymodules.Fees{
				ContractFees:    types.SiacoinPrecision.Mul64(2),
				SiafundFees:     types.SiacoinPrecision.Mul64(3),
				TransactionFees: types.SiacoinPrecision.Mul64(4),
			},
			FundAccountSpending: types.SiacoinPrecision.Mul64(5),
			MaintenanceSpending: skymodules.MaintenanceSpending{
				AccountBalanceCost:   types.SiacoinPrecision.Mul64(6),
				FundAccountCost:      types.SiacoinPrecision.Mul64(7),
				UpdatePriceTableCost: types.SiacoinPrecision.Mul64(8),
			},
			StorageSpending:            types.SiacoinPrecision.Mul64(9),
			TotalAllocated:             types.SiacoinPrecision.Mul64(10),
			UploadSpending:             types.SiacoinPrecision.Mul64(11),
			Unspent:                    types.SiacoinPrecision.Mul64(12),
			ContractSpendingDeprecated: types.SiacoinPrecision.Mul64(13),
			WithheldFunds:              types.SiacoinPrecision.Mul64(14),
			ReleaseBlock:               types.BlockHeight(1),
			PreviousSpending:           types.SiacoinPrecision.Mul64(15),
		},
		EphemeralAccountSpending: []skymodules.EphemeralAccountSpending{
			{
				AccountSpending: skymodules.AccountSpending{
					AccountBalanceCost:    types.SiacoinPrecision.Mul64(16),
					DownloadsCost:         types.SiacoinPrecision.Mul64(17),
					RegistryReadsCost:     types.SiacoinPrecision.Mul64(18),
					RegistryWritesCost:    types.SiacoinPrecision.Mul64(19),
					RepairDownloadsCost:   types.SiacoinPrecision.Mul64(20),
					RepairUploadsCost:     types.SiacoinPrecision.Mul64(21),
					SnapshotDownloadsCost: types.SiacoinPrecision.Mul64(22),
					SnapshotUploadsCost:   types.SiacoinPrecision.Mul64(23),
					SubscriptionsCost:     types.SiacoinPrecision.Mul64(24),
					UpdatePriceTableCost:  types.SiacoinPrecision.Mul64(25),
					UploadsCost:           types.SiacoinPrecision.Mul64(26),

					Balance:      types.SiacoinPrecision.Mul64(27),
					BalanceDrift: *big.NewInt(-28),
					Residue:      types.SiacoinPrecision.Mul64(29),
				},
				HostKey: types.SiaPublicKey{},
			},
		},
		SkynetFee: types.SiacoinPrecision.Mul64(28),
	}

	expected := `
    Spent Funds:               56 SC
      Storage:                 9 SC
      Upload:                  11 SC
      Download:                1 SC
      FundAccount:             5 SC (+29 SC residue)
        AccountBalanceCost:    16 SC
        Balance:               27 SC (-28 H drift)
        DownloadsCost:         17 SC
        MiscCost:              45 SC
        RegistryReadsCost:     18 SC
        RegistryWritesCost:    19 SC
        RepairsCost:           41 SC
        SubscriptionsCost:     24 SC
        UpdatePriceTableCost:  25 SC
        UploadsCost:           26 SC
        Drift:                 -282 SC
      Maintenance:             21 SC
        AccountBalanceCost:    6 SC
        FundAccountCost:       7 SC
        UpdatePriceTableCost:  8 SC
      Fees:                    9 SC
        ContractFees:          2 SC
        SiafundFees:           3 SC
        TransactionFees:       4 SC
    Unspent Funds:             12 SC
      Allocated:               0 H
      Unallocated:             12 SC
    Skynet Fee:                28 SC
`
	actual := currentperiodspending(fm, nil)
	if actual != expected {
		t.Fatal(actual, expected, diff.LineDiff(expected, actual))
	}
}
