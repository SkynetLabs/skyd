package build

var (
	// maxDownloadDiskCacheSize is the environment variable for the on-disk
	// download cache size.
	maxDownloadDiskCacheSize = "SKYD_DISK_CACHE_SIZE"

	// minDownloadDiskCacheHits is the number of times a section of a
	// datasource needs to be downloaded before being cached.
	minDownloadDiskCacheHits = "SKYD_DISK_CACHE_MIN_HITS"

	// downloadDiskCacheHitDuration is the period of time in seconds within
	// which the minDownloadDiskCacheHits need to occur for a section to be
	// cached.
	downloadDiskCacheHitDuration = "SKYD_DISK_CACHE_HIT_PERIOD"

	// diskCacheDisabled is the environment variable to enable the on-disk
	// cache for skyd.
	diskCacheEnabled = "SKYD_DISK_CACHE_ENABLED"

	// mongoDBURI is the environment variable for the mongodb URI.
	mongoDBURI = "MONGODB_URI"

	// mongoDBUser is the environment variable for the mongodb user.
	mongoDBUser = "MONGODB_USER"

	// mongoDBPassword is the environment variable for the mongodb password.
	mongoDBPassword = "MONGODB_PASSWORD"

	// serverUID is the unique identifier of the server within a portal
	// cluster.
	serverUID = "SERVER_UID"

	// siaAPIPassword is the environment variable that sets a custom API
	// password if the default is not used
	siaAPIPassword = "SIA_API_PASSWORD"

	// siaDataDir is the environment variable that tells siad where to put the
	// general sia data, e.g. api password, configuration, logs, etc.
	siaDataDir = "SIA_DATA_DIR"

	// siadDataDir is the environment variable which tells siad where to put the
	// siad-specific data
	siadDataDir = "SIAD_DATA_DIR"

	// siaWalletPassword is the environment variable that can be set to enable
	// auto unlocking the wallet
	siaWalletPassword = "SIA_WALLET_PASSWORD"

	// siaExchangeRate is the environment variable that can be set to
	// show amounts (additionally) in a different currency
	siaExchangeRate = "SIA_EXCHANGE_RATE"

	// skydLogLevel determines the log level for skyd.
	//
	// Currently this only checks for DEBUG as this is the only non default
	// log level available for the skyd logger.
	skydLogLevel = "SKYD_LOG_LEVEL"

	// tusMaxSize determines the max size of an upload via the /tus endpoint.
	tusMaxSize = "TUS_MAXSIZE"
)
