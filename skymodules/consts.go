package skymodules

import "time"

const (
	// StackSize is the size of the buffer used to store the stack trace.
	StackSize = 64e6 // 64MB
)

// DaysInMonth is a helper for getting the days in a given month. This utilizes
// the built in functionality of the time package the normalizes the 0th day of
// the month as the last day of the previous month.
func DaysInMonth(month time.Month, year int) int {
	return time.Date(year, month+1, 0, 0, 0, 0, 0, time.UTC).Day()
}
