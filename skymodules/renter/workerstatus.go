package renter

import (
	"time"

	"gitlab.com/SkynetLabs/skyd/skymodules"
)

// callStatus returns the status of the worker.
func (w *worker) callStatus() skymodules.WorkerStatus {
	// Fetch cool down info on all queues of interest
	downloadOnCD, downloadTerminated, downloadQueueSize, downloadCDTime, downloadCDErr := w.staticJobReadQueue.callCooldownStatus()
	hasSectorOnCD, hasSectorTerminated, hasSectorQueueSize, hasSectorCDTime, hasSectorCDErr := w.staticJobHasSectorQueue.callCooldownStatus()
	lowPrioOnCD, lowPrioTerminated, lowPrioQueueSize, lowPrioCDTime, lowPrioCDErr := w.staticJobLowPrioReadQueue.callCooldownStatus()

	w.mu.Lock()
	defer w.mu.Unlock()

	uploadOnCoolDown, uploadCoolDownTime := w.onUploadCooldown()
	var uploadCoolDownErr string
	if w.uploadRecentFailureErr != nil {
		uploadCoolDownErr = w.uploadRecentFailureErr.Error()
	}

	maintenanceOnCooldown, maintenanceCoolDownTime, maintenanceCoolDownErr := w.staticMaintenanceState.managedMaintenanceCooldownStatus()
	var maintenanceCoolDownErrStr string
	if maintenanceCoolDownErr != nil {
		maintenanceCoolDownErrStr = maintenanceCoolDownErr.Error()
	}

	// Update the worker cache before returning a status.
	w.staticTryUpdateCache()
	cache := w.staticCache()
	return skymodules.WorkerStatus{
		// Contract Information
		ContractID:      cache.staticContractID,
		ContractUtility: cache.staticContractUtility,
		HostPubKey:      w.staticHostPubKey,

		// Download information
		DownloadCoolDownError: downloadCDErr,
		DownloadCoolDownTime:  downloadCDTime,
		DownloadOnCoolDown:    downloadOnCD,
		DownloadQueueSize:     downloadQueueSize,
		DownloadTerminated:    downloadTerminated,

		// Has Sector information
		HasSectorCoolDownError: hasSectorCDErr,
		HasSectorCoolDownTime:  hasSectorCDTime,
		HasSectorOnCoolDown:    hasSectorOnCD,
		HasSectorQueueSize:     hasSectorQueueSize,
		HasSectorTerminated:    hasSectorTerminated,

		// Low Prio Download (repairs) information
		LowPrioDownloadCoolDownError: lowPrioCDErr,
		LowPrioDownloadCoolDownTime:  lowPrioCDTime,
		LowPrioDownloadOnCoolDown:    lowPrioOnCD,
		LowPrioDownloadQueueSize:     lowPrioQueueSize,
		LowPrioDownloadTerminated:    lowPrioTerminated,

		// Upload information
		UploadCoolDownError: uploadCoolDownErr,
		UploadCoolDownTime:  uploadCoolDownTime,
		UploadOnCoolDown:    uploadOnCoolDown,
		UploadQueueSize:     w.unprocessedChunks.Len(),
		UploadTerminated:    w.uploadTerminated,

		// Job Queues
		DownloadSnapshotJobQueueSize: int(w.staticJobDownloadSnapshotQueue.callStatus().size),
		UploadSnapshotJobQueueSize:   int(w.staticJobUploadSnapshotQueue.callStatus().size),

		// Maintenance Cooldown Information
		MaintenanceOnCooldown:    maintenanceOnCooldown,
		MaintenanceCoolDownError: maintenanceCoolDownErrStr,
		MaintenanceCoolDownTime:  maintenanceCoolDownTime,

		// Account Information
		AccountBalanceTarget: w.staticRenter.staticAccountBalanceTarget,
		AccountStatus:        w.staticAccount.managedStatus(),

		// Price Table Information
		PriceTableStatus: w.staticPriceTableStatus(),

		// Read Job Information
		ReadJobsStatus: w.callReadJobStatus(),

		// HasSector Job Information
		HasSectorJobsStatus: w.callHasSectorJobStatus(),

		// ReadRegistry Job Information
		ReadRegistryJobsStatus: w.callReadRegistryJobsStatus(),

		// UpdateRegistry Job Information
		UpdateRegistryJobsStatus: w.callUpdateRegistryJobsStatus(),
	}
}

// staticPriceTableStatus returns the status of the worker's price table
func (w *worker) staticPriceTableStatus() skymodules.WorkerPriceTableStatus {
	pt := w.staticPriceTable()

	var recentErrStr string
	if pt.staticRecentErr != nil {
		recentErrStr = pt.staticRecentErr.Error()
	}

	return skymodules.WorkerPriceTableStatus{
		ExpiryTime: pt.staticExpiryTime,
		UpdateTime: pt.staticUpdateTime,

		Active: time.Now().Before(pt.staticExpiryTime),

		RecentErr:     recentErrStr,
		RecentErrTime: pt.staticRecentErrTime,
	}
}

// callReadJobStatus returns the status of the read job queue
func (w *worker) callReadJobStatus() skymodules.WorkerReadJobsStatus {
	jrq := w.staticJobReadQueue
	status := jrq.callStatus()

	var recentErrString string
	if status.recentErr != nil {
		recentErrString = status.recentErr.Error()
	}

	avgJobTimeInMs := func(l uint64) uint64 {
		if d := jrq.staticStats.callExpectedJobTime(l); d > 0 {
			return uint64(d.Milliseconds())
		}
		return 0
	}

	return skymodules.WorkerReadJobsStatus{
		AvgJobTime64k:       avgJobTimeInMs(1 << 16),
		AvgJobTime1m:        avgJobTimeInMs(1 << 20),
		AvgJobTime4m:        avgJobTimeInMs(1 << 22),
		ConsecutiveFailures: status.consecutiveFailures,
		JobQueueSize:        status.size,
		RecentErr:           recentErrString,
		RecentErrTime:       status.recentErrTime,
	}
}

// callHasSectorJobStatus returns the status of the has sector job queue
func (w *worker) callHasSectorJobStatus() skymodules.WorkerHasSectorJobsStatus {
	hsq := w.staticJobHasSectorQueue
	status := hsq.callStatus()

	var recentErrStr string
	if status.recentErr != nil {
		recentErrStr = status.recentErr.Error()
	}

	avgJobTimeInMs := uint64(hsq.callExpectedJobTime().Milliseconds())

	return skymodules.WorkerHasSectorJobsStatus{
		AvgJobTime:          avgJobTimeInMs,
		ConsecutiveFailures: status.consecutiveFailures,
		JobQueueSize:        status.size,
		RecentErr:           recentErrStr,
		RecentErrTime:       status.recentErrTime,
	}
}

// callGenericWorkerJobStatus returns the status for the generic job queue.
func callGenericWorkerJobStatus(queue *jobGenericQueue) skymodules.WorkerGenericJobsStatus {
	status := queue.callStatus()

	var recentErrStr string
	if status.recentErr != nil {
		recentErrStr = status.recentErr.Error()
	}

	return skymodules.WorkerGenericJobsStatus{
		ConsecutiveFailures: status.consecutiveFailures,
		JobQueueSize:        status.size,
		OnCooldown:          time.Now().Before(status.cooldownUntil),
		OnCooldownUntil:     status.cooldownUntil,
		RecentErr:           recentErrStr,
		RecentErrTime:       status.recentErrTime,
	}
}

// callUpdateRegistryJobsStatus returns the status for the ReadRegistry queue.
func (w *worker) callReadRegistryJobsStatus() skymodules.WorkerReadRegistryJobStatus {
	return skymodules.WorkerReadRegistryJobStatus{
		WorkerGenericJobsStatus: callGenericWorkerJobStatus(w.staticJobReadRegistryQueue.jobGenericQueue),
	}
}

// callUpdateRegistryJobsStatus returns the status for the UpdateRegistry queue.
func (w *worker) callUpdateRegistryJobsStatus() skymodules.WorkerUpdateRegistryJobStatus {
	return skymodules.WorkerUpdateRegistryJobStatus{
		WorkerGenericJobsStatus: callGenericWorkerJobStatus(w.staticJobUpdateRegistryQueue.jobGenericQueue),
	}
}
