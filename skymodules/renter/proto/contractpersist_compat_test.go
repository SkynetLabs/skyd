package proto

import (
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/NebulousLabs/encoding"
)

var (
	v1412ContractLocation = filepath.Join("testdata", "v1412.contract")
	v1420ContractLocation = filepath.Join("testdata", "v1420.header")
)

// TestLoadV1412Contract will test that loading a v1412 legacy contract is
// successful.
func TestLoadV1412Contract(t *testing.T) {
	f, err := os.Open(v1412ContractLocation)
	if err != nil {
		t.Fatal(err)
	}
	stat, err := f.Stat()
	if err != nil {
		t.Fatal(err)
	}
	decodeMaxSize := int(stat.Size() * decodeMaxSizeMultiplier)

	_, err = loadSafeContractHeader(f, decodeMaxSize)
	if err != nil {
		t.Fatal(err)
	}
}

// TestLoadV160Contract will test that loading a v160 legacy contract is
// successful.
func TestLoadV160Contract(t *testing.T) {
	f, err := os.Open(v1420ContractLocation)
	if err != nil {
		t.Fatal(err)
	}
	stat, err := f.Stat()
	if err != nil {
		t.Fatal(err)
	}
	decodeMaxSize := int(stat.Size() * decodeMaxSizeMultiplier)

	_, err = loadSafeContractHeader(f, decodeMaxSize)
	if err != nil {
		t.Fatal(err)
	}
}

// TestLoadV1420SetHeaderUpdate will test that loading a v1420 legacy header
// update is successful.
func TestLoadV1420SetHeaderUpdate(t *testing.T) {
	var update v160UpdateSetHeader
	update.Header.Utility.GoodForRenew = true
	b := encoding.Marshal(update)

	var newUpdate updateSetHeader
	err := updateSetHeaderUnmarshalV1420ToV160(b, &newUpdate)
	if err != nil {
		t.Fatal(err)
	}
	if newUpdate.Header.Utility.GoodForRefresh != true {
		t.Fatal("goodForRefresh should be true")
	}
}
