package renter

import (
	"bytes"
	"context"
	"time"

	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"gitlab.com/SkynetLabs/skyd/skymodules/gouging"
	"go.sia.tech/siad/types"
)

type (
	// jobDownloadSnapshot is a job for the worker to download a snapshot from
	// its respective host.
	jobDownloadSnapshot struct {
		staticResponseChan chan *jobDownloadSnapshotResponse

		jobGeneric
	}

	// jobDownloadSnapshotQueue contains the download jobs.
	jobDownloadSnapshotQueue struct {
		*jobGenericQueue
	}

	// jobDownloadSnapshotResponse contains the response to an upload snapshot
	// job.
	jobDownloadSnapshotResponse struct {
		staticSnapshots []snapshotEntry
		staticErr       error
	}
)

// callDiscard will discard this job, sending an error down the response
// channel.
func (j *jobDownloadSnapshot) callDiscard(err error) {
	resp := &jobDownloadSnapshotResponse{
		staticErr: err,
	}
	w := j.staticQueue.staticWorker()
	w.staticTG.Launch(func() {
		select {
		case j.staticResponseChan <- resp:
		case <-j.staticCtx.Done():
		case <-w.staticTG.StopChan():
		}
	})
}

// callExecute will perform an upload snapshot job for the worker.
func (j *jobDownloadSnapshot) callExecute() (err error) {
	w := j.staticQueue.staticWorker()

	// Defer a function to send the result down a channel.
	var snapshots []snapshotEntry
	defer func() {
		resp := &jobDownloadSnapshotResponse{
			staticErr:       err,
			staticSnapshots: snapshots,
		}
		w.staticTG.Launch(func() {
			select {
			case j.staticResponseChan <- resp:
			case <-j.staticCtx.Done():
			case <-w.staticTG.StopChan():
			}
		})

		// Report a failure to the queue if this job had an error.
		if resp.staticErr != nil {
			j.staticQueue.callReportFailure(resp.staticErr, j.externExecuteTime, time.Now())
		} else {
			j.staticQueue.callReportSuccess()
		}
	}()

	// Check for gouging
	allowance := w.staticCache().staticRenterAllowance
	pt := w.staticPriceTable().staticPriceTable
	err = gouging.CheckSnapshot(allowance, pt)
	if err != nil {
		err = errors.AddContext(err, "price gouging check failed for download snapshot job")
		return
	}

	// Perform the actual download
	snapshots, err = w.staticRenter.managedDownloadSnapshotTable(w)
	if err != nil && errors.Contains(err, errEmptyContract) {
		err = nil
	}

	return
}

// callExpectedBandwidth returns the amount of bandwidth this job is expected to
// consume.
func (j *jobDownloadSnapshot) callExpectedBandwidth() (ul, dl uint64) {
	// Estimate 50kb in overhead for upload and download, and then 4 MiB
	// necessary to send the actual full sector payload.
	return 50e3 + 1<<22, 50e3
}

// initJobUploadSnapshotQueue will initialize the upload snapshot job queue for
// the worker.
func (w *worker) initJobDownloadSnapshotQueue() {
	if w.staticJobDownloadSnapshotQueue != nil {
		w.staticRenter.staticLog.Critical("should not be double initializng the upload snapshot queue")
		return
	}

	w.staticJobDownloadSnapshotQueue = &jobDownloadSnapshotQueue{
		jobGenericQueue: newJobGenericQueue(w),
	}
}

// DownloadSnapshotTable is a convenience method to runs a DownloadSnapshot job
// on a worker and returns the snapshot table.
func (w *worker) DownloadSnapshotTable(ctx context.Context) ([]snapshotEntry, error) {
	downloadSnapshotRespChan := make(chan *jobDownloadSnapshotResponse)
	jus := &jobDownloadSnapshot{
		staticResponseChan: downloadSnapshotRespChan,

		jobGeneric: newJobGeneric(ctx, w.staticJobDownloadSnapshotQueue, nil),
	}

	// Add the job to the queue.
	if !w.staticJobDownloadSnapshotQueue.callAdd(jus) {
		return nil, errors.New("worker unavailable")
	}

	// Wait for the response.
	var resp *jobDownloadSnapshotResponse
	select {
	case <-ctx.Done():
		return nil, errors.New("DownloadSnapshotTable interrupted")
	case resp = <-downloadSnapshotRespChan:
	}

	if resp.staticErr != nil {
		return nil, errors.AddContext(resp.staticErr, "DownloadSnapshotTable failed")
	}
	return resp.staticSnapshots, nil
}

// FetchBackups is a convenience method to runs a DownloadSnapshot job on a
// worker and formats the response to a list of UploadedBackup objects.
func (w *worker) FetchBackups(ctx context.Context) ([]skymodules.UploadedBackup, error) {
	snapshots, err := w.DownloadSnapshotTable(ctx)
	if err != nil {
		return nil, errors.AddContext(err, "FetchBackups failed to download snapshot table")
	}

	// Format the response and return the response to the requester.
	uploadedBackups := make([]skymodules.UploadedBackup, len(snapshots))
	for i, e := range snapshots {
		uploadedBackups[i] = skymodules.UploadedBackup{
			Name:           string(bytes.TrimRight(e.Name[:], types.RuneToString(0))),
			UID:            e.UID,
			CreationDate:   e.CreationDate,
			Size:           e.Size,
			UploadProgress: 100,
		}
	}
	return uploadedBackups, nil
}
