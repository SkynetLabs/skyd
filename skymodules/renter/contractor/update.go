package contractor

import (
	"gitlab.com/NebulousLabs/fastrand"

	"gitlab.com/SkynetLabs/skyd/build"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/crypto"
	"go.sia.tech/siad/modules"
	"go.sia.tech/siad/types"
)

// hasFCIdentifier checks the transaction for a ContractSignedIdentifier and
// returns the first one it finds with a bool indicating if an identifier was
// found.
func hasFCIdentifier(txn types.Transaction) (skymodules.ContractSignedIdentifier, crypto.Ciphertext, bool) {
	// We don't verify the host key here so we only need to make sure the
	// identifier fits into the arbitrary data.
	if len(txn.ArbitraryData) != 1 || len(txn.ArbitraryData[0]) < skymodules.FCSignedIdentiferSize {
		return skymodules.ContractSignedIdentifier{}, nil, false
	}
	// Verify the prefix.
	var prefix types.Specifier
	copy(prefix[:], txn.ArbitraryData[0])
	if prefix != modules.PrefixFileContractIdentifier &&
		prefix != modules.PrefixNonSia {
		return skymodules.ContractSignedIdentifier{}, nil, false
	}
	// We found an identifier.
	var csi skymodules.ContractSignedIdentifier
	n := copy(csi[:], txn.ArbitraryData[0])
	hostKey := txn.ArbitraryData[0][n:]
	return csi, hostKey, true
}

// HostRevisionSubmissionBuffer describes the number of blocks ahead of time
// that the host will submit a file contract revision. The host will not
// accept any more revisions once inside the submission buffer.
var HostRevisionSubmissionBuffer = build.Select(build.Var{
	Dev:      types.BlockHeight(20),  // About 4 minutes
	Standard: types.BlockHeight(144), // 1 day.
	Testing:  types.BlockHeight(4),
}).(types.BlockHeight)

// contractsToDelete returns how many contracts of numContracts potential
// candidates should be deleted within managedArchiveContracts. It returns a
// number between 0 and 1% of numContracts.
// 1% was chosen since we want to delete all contracts eventually within 1 day
// and scales well for larger numbers of contracts since we will delete more
// contracts at first and fewer ones in subsequent calls.
func contractsToDelete(numContracts int) int {
	toDelete := int(float64(numContracts) * 0.01)
	if numContracts > 0 && toDelete == 0 {
		return 1 // delete at least 1
	}
	return toDelete
}

// managedArchiveContracts will figure out which contracts are no longer needed
// and move them to the historic set of contracts.
func (c *Contractor) managedArchiveContracts() {
	// Determine the current block height.
	c.mu.RLock()
	currentHeight := c.blockHeight
	c.mu.RUnlock()

	// Loop through the current set of contracts and migrate any expired ones to
	// the set of old contracts.
	var expired []types.FileContractID
	var potentialDeletes []skymodules.RenterContract
	for _, contract := range c.staticContracts.ViewAll() {
		// Check map of renewedTo in case renew code was interrupted before
		// archiving old contract
		c.mu.RLock()
		_, renewed := c.renewedTo[contract.ID]
		c.mu.RUnlock()
		if currentHeight+HostRevisionSubmissionBuffer > contract.EndHeight || renewed {
			id := contract.ID
			c.mu.Lock()
			c.oldContracts[id] = contract
			c.mu.Unlock()
			expired = append(expired, id)
			c.staticLog.Println("INFO: archived renewed contract", id)
		} else if !c.staticDeps.Disrupt("DisableEarlyContractArchival") && currentHeight+2*HostRevisionSubmissionBuffer > contract.EndHeight {
			potentialDeletes = append(potentialDeletes, contract)
		}
	}

	// Delete a part of the potential candidates for deletion.
	toDelete := contractsToDelete(len(potentialDeletes))
	for _, contract := range potentialDeletes[:toDelete] {
		id := contract.ID
		c.mu.Lock()
		c.oldContracts[id] = contract
		c.mu.Unlock()
		expired = append(expired, id)
		c.staticLog.Println("INFO: archived expired contract", id)
	}

	// Save.
	c.mu.Lock()
	c.save()
	c.mu.Unlock()

	// Delete all the expired contracts from the contract set.
	for _, id := range expired {
		if sc, ok := c.staticContracts.Acquire(id); ok {
			c.staticContracts.Delete(sc)
		}
	}
}

// ProcessConsensusChange will be called by the consensus set every time there
// is a change in the blockchain. Updates will always be called in order.
func (c *Contractor) ProcessConsensusChange(cc modules.ConsensusChange) {
	// Get the wallet's seed for contract recovery.
	haveSeed := true
	missedRecovery := false
	s, _, err := c.staticWallet.PrimarySeed()
	if err != nil {
		haveSeed = false
	}
	// Get the master renter seed and wipe it once we are done with it.
	var renterSeed skymodules.RenterSeed
	if haveSeed {
		renterSeed = skymodules.DeriveRenterSeed(s)
		defer fastrand.Read(renterSeed[:])
	}

	c.mu.Lock()
	for _, block := range cc.RevertedBlocks {
		if block.ID() != types.GenesisID {
			c.blockHeight--
		}
		// Remove recoverable contracts found in reverted block.
		c.removeRecoverableContracts(block)
	}
	for _, block := range cc.AppliedBlocks {
		if block.ID() != types.GenesisID {
			c.blockHeight++
		}
		// Find lost contracts for recovery.
		if haveSeed {
			c.findRecoverableContracts(renterSeed, block)
		} else {
			missedRecovery = true
		}
	}
	c.staticWatchdog.callScanConsensusChange(cc)

	// If we didn't miss the recover, we update the recentRecoverChange
	if !missedRecovery && c.recentRecoveryChange == c.lastChange {
		c.recentRecoveryChange = cc.ID
	}

	// If the allowance is set and we have entered the next period, update
	// currentPeriod.
	if c.allowance.Active() && c.blockHeight >= c.currentPeriod+c.allowance.Period {
		c.currentPeriod += c.allowance.Period
		c.staticChurnLimiter.callResetAggregateChurn()

		// COMPATv1.0.4-lts
		// if we were storing a special metrics contract, it will be invalid
		// after we enter the next period.
		delete(c.oldContracts, metricsContractID)
	}

	// Check if c.synced already signals that the contractor is synced.
	synced := false
	select {
	case <-c.synced:
		synced = true
	default:
	}
	// If we weren't synced but are now, we close the channel. If we were
	// synced but aren't anymore, we need a new channel.
	if !synced && cc.Synced {
		close(c.synced)
	} else if synced && !cc.Synced {
		c.synced = make(chan struct{})
	}
	// Let the watchdog take any necessary actions and update its state. We do
	// this before persisting the contractor so that the watchdog is up-to-date on
	// reboot. Otherwise it is possible that e.g. that the watchdog thinks a
	// storage proof was missed and marks down a host for that. Other watchdog
	// actions are innocuous.
	if cc.Synced {
		c.staticWatchdog.callCheckContracts()
	}

	c.lastChange = cc.ID
	err = c.save()
	if err != nil {
		c.staticLog.Println("Unable to save while processing a consensus change:", err)
	}
	a := c.allowance
	c.mu.Unlock()

	// Add to churnLimiter budget.
	numBlocksAdded := len(cc.AppliedBlocks) - len(cc.RevertedBlocks)
	c.staticChurnLimiter.callBumpChurnBudget(numBlocksAdded, a.Period)

	// Perform contract maintenance if our blockchain is synced. Use a separate
	// goroutine so that the rest of the contractor is not blocked during
	// maintenance.
	if cc.Synced {
		go c.threadedContractMaintenance()
	}
}
